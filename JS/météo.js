let ville = "Paris";

function choisirVille() {
  const url =
    "https://api.openweathermap.org/data/2.5/weather?q=" +
    ville +
    "&appid=ba16537b8a0dea81d04cb1a4f5dd9212&units=metric";
  ///////////////////////////
  let req = new XMLHttpRequest();
  req.open("GET", url);
  req.responseType = "json";
  req.send();
  ///////////////////////////
  req.onload = function () {
    if (req.readyState === XMLHttpRequest.DONE) {
      if (req.status === 200) {
        let villeDiv = document.querySelector("#ville");
        let temperatureDiv = document.querySelector("#temperature");
        let image = document.querySelector("img");
        let resultat = document.querySelector("#result");
        /////////////////////////////
        let response = req.response;
        let temperature = response.main.temp;
        let ville = response.name;
        let icon = response.weather[0].icon;
        let DescriptionWeather = response.weather[0].main;
        let URLIcon = "http://openweathermap.org/img/wn/" + icon + "@2x.png";
        /////////////////////////////
        image.setAttribute("src", URLIcon);
        /////////////////////////////
        villeDiv.textContent = ville;
        temperatureDiv.textContent = temperature + "°C";
        console.log(response)
        ////////////////////////////
        if (DescriptionWeather === "Clear") {
          resultat.className = "soleil";
        } else if (DescriptionWeather === "Clouds") {
          resultat.className = "nuageux";
        } else if (
          DescriptionWeather === "Drizzle" ||
          DescriptionWeather === "Rain"
        ) {
          resultat.className ="pluie";
        } else if (DescriptionWeather === "Thunderstorm") {
          resultat.className = "foudre";
        } else if (DescriptionWeather === "Snow") {
          resultat.className = "neige";
        }else{
          resultat.className="resultat"
        }

      } 
      /////////////////////////////////////////////////
    }
  };
}



/////////////////////////////////////////
choisirVille();
/////////////////////////////////////////
let boutonDiv = document.querySelector("#bouton");
boutonDiv.addEventListener("click", () => {
  ville = prompt("Choisis ta ville :)");
  choisirVille();
});
